package productApiTests;

import constants.Products;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import products.SendPro360;
import utils.TestData;

public class E2eTests {

    private TestData testData;
    private Logger logger = LogManager.getLogger(E2eTests.class);


    @Test
    public void sp360ukWorkflow() throws Exception {
        logger.info("Starting test : sp360ukWorkflow");
        testData = new TestData("SendPro360_UK_Workflow_201.json", Products.SEND_PRO_360);
        JSONObject payload = testData.getJsonBody();

        Response response = SendPro360.createSubscriptionV3(payload);
    }
}
