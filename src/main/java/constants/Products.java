package constants;

public final class Products {
    public static final String SEND_PRO_360 = "SendPro360";
    public static final String PRODUCT_NOTIFICATION_ACKNOWLEDGEMENT = "ProductNotificationAcknowledgement";
}
