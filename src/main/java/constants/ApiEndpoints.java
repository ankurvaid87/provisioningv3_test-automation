package constants;

public class ApiEndpoints {
    public static final String PROVISIONING_V3_BASE_URL = "https://ccbs-provisioningv3-qa.saase2e.pitneycloud.com/services/v3";
    public static final String CREATE_SUBSCRIPTION = "/createSubscription";
    public static final String GET_JOB_STATUS = "/jobStatus";
    public static final String GET_JOB_STATUS_DEBUG = "/jobStatusDebug";
    public static final String PRODUCT_NOTIFICATION_ACKNOWLEGDEMENT = "/notificationEventAcknowledgement";
    public static final String OAUTH_BASE_URL = "https://api-qa.pitneybowes.com/";
    public static final String OAUTH_ENDPOINT = "/oauth/token";
    public static final String GUAM_BASE_URL = "https://ccbs-guam-v3-qa.saase2e.pitneycloud.com/apis/services/v3";

}
