package products;

import apis.Guam;
import apis.ProvisioningV3;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import java.io.IOException;
import java.net.HttpURLConnection;

public class SendPro360 {

    private static String processId;
    private static String errorCode;
    private static String errorDescription;
    private static final Logger logger = LogManager.getLogger(SendPro360.class);


    /**
     * Description : Executes a POST request to Workflow Provisioning V3 with default headers, asserts status code (Expected : 201) and logs errors (if any)
     * @param jsonBody
     * @return Response from /create
     * @throws IOException
     * @throws ParseException
     */
    public static Response createSubscriptionV3(JSONObject jsonBody) throws Exception {
        Response response = ProvisioningV3.createSubscriptionWorkflow(jsonBody);

        Assert.assertTrue(response.statusCode() == HttpURLConnection.HTTP_CREATED,
                "ProvisioningV3.createSubscriptionWorkflow did not return 201 response");

        return response;
    }
}
