package apis;

import constants.ApiEndpoints;
import constants.HeaderType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
import utils.ApiHelper;

import java.io.IOException;
import java.util.HashMap;

public class Authorization {
    private Logger logger;

    public Authorization(){
        logger = LogManager.getLogger(Authorization.class);
    }

    /**
     * @author : Ankur Vaid
     * Description : This method executes a POST request to OAuth API and returns access-token
     * @return Bearer access token from OAuth API
     * @throws IOException
     * @throws ParseException
     */
    public String getOauthToken() throws Exception {
        logger.info("Getting OAuth Token from OAuth API...");
        HashMap<String, String> formParams = new HashMap<>();
        formParams.put("grant_type", "client_credentials");
        Response response = new Request(ApiEndpoints.OAUTH_BASE_URL)
                .setEndPoint(ApiEndpoints.OAUTH_ENDPOINT)
                .setMethod(Method.POST)
                .setHeader(HeaderType.CONTENT_TYPE, "application/x-www-form-urlencoded")
                .setHeader("Accept-Encoding", "gzip, deflate, br")
                .setHeader("Connection", "keep-alive")
                .setHeader(HeaderType.AUTHORIZATION, ApiHelper.getBasicAuth())
                .setBodyParams(formParams)
                .execute();

        String accessToken = "Bearer " + response.jsonPath().getString("access_token");
        logger.debug("/oauth/token response is : \n" + response.body().prettyPrint());

        return accessToken;
    }
}
