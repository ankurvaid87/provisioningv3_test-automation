package apis;

import constants.ApiEndpoints;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
import utils.ApiHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Guam {

    private static Logger logger = LogManager.getLogger(Guam.class);

    /**
     * @author : Ankur Vaid
     * Description : This method gets the subscription details of a user from Guam
     * @param userEmail
     * @return Response from Guam API
     * @throws IOException
     * @throws ParseException
     */
    public static Response getSubscriptionDetails(String userEmail) throws Exception {
        logger.info("Getting subscription Details from Guam...");
        Response response = new Request(ApiEndpoints.GUAM_BASE_URL)
                .setEndPoint("users/" + userEmail + "/info")
                .setBasicAuth(ApiHelper.getGUAMusername(), ApiHelper.getGUAMpassword())
                .setMethod(Method.GET)
                .execute();

        logger.debug("Subscription details for user " + userEmail + " : \n" + response.body().prettyPrint());
        return response;
    }

    private static LinkedHashMap getSubscription(Response guamResponse){
        LinkedHashMap entitlements = guamResponse.body().jsonPath().get("entitlements");
        ArrayList subscriptionMemberships = (ArrayList) entitlements.get("subscriptionMemberships");
        LinkedHashMap subscription = (LinkedHashMap)((LinkedHashMap)subscriptionMemberships.get(0)).get("subscription");

        return subscription;
    }

    public static String getSubscriptionId(Response guamResponse){
        return getSubscription(guamResponse).get("subscriptionId").toString();
    }
}
