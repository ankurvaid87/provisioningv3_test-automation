package apis;

import constants.ApiEndpoints;
import constants.HeaderType;
import io.restassured.http.Method;
import io.restassured.internal.ResponseParserRegistrar;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import utils.ApiHelper;
import utils.TestData;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

public class ProvisioningV3 {

    private static final Logger logger = LogManager.getLogger(ProvisioningV3.class);

    /**
     * Creates and returns default headers for workflow create subscription request as a key-value pair (HashMap)
     * @return
     * @throws IOException
     * @throws ParseException
     */
    private static HashMap setDefaultHeadersWorkflow() throws IOException, ParseException {
        String debugHeaders = "Request Headers ->  ";

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(HeaderType.CONTENT_TYPE, "application/json");
        debugHeaders += "Content-Type : " + headers.get("Content-Type") + "\t";
        headers.put(HeaderType.AUTHORIZATION, ApiHelper.getBasicAuth());
        debugHeaders += "Authorization : " + headers.get("Authorization") + "\t";
        headers.put(HeaderType.X_PB_TRANSACTION_ID, TestData.generateRandomTransactionId());
        debugHeaders += "X-PB-TransactionID : " + headers.get("X-PB-TransactionID");

        logger.debug(debugHeaders);

        return headers;
    }

    /**
     * Creates and returns default headers for APIM (Apigee) create subscription request as a key-value pair (HashMap)
     * @return
     * @throws IOException
     * @throws ParseException
     */
    private static HashMap setDefaultHeadersAPIM() throws Exception {
        Authorization authorization = new Authorization();

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(HeaderType.CONTENT_TYPE, "application/json");
        headers.put(HeaderType.AUTHORIZATION, authorization.getOauthToken());
        headers.put(HeaderType.X_PB_TRANSACTION_ID, TestData.generateRandomTransactionId());

        return headers;
    }

    /**
     * @author : Ankur Vaid
     * Description : Executes a POST Request for create subscription with user defined headers
     * @param jsonBody
     * @param headers
     * @return Response from create endpoint of v3 Provisioning API
     * @throws IOException
     * @throws ParseException
     */
    public static Response createSubscription(JSONObject jsonBody, Map<String, String> headers) throws Exception {
        Response response = new Request(ApiEndpoints.PROVISIONING_V3_BASE_URL)
                .setEndPoint(ApiEndpoints.CREATE_SUBSCRIPTION)
                .setMethod(Method.POST)
                .setHeader(headers)
                .setBodyJson(jsonBody)
                .execute();

        return response;
    }

    /**
     * @author : Ankur Vaid
     * Description : Executes a POST Request for workflow create subscription by calling createSubscription method with default headers
     * @param jsonBody
     * @return Response from create endpoint of v3 Provisioning API
     * @throws IOException
     * @throws ParseException
     */
    public static Response createSubscriptionWorkflow(JSONObject jsonBody) throws Exception {
        return createSubscription(jsonBody, setDefaultHeadersWorkflow());
    }

    /**
     * @author : Ankur Vaid
     * Description : Executes a POST Request for APIM (Apigee) create subscription by calling createSubscription method with default headers
     * @param jsonBody
     * @return Response from create endpoint of v2.5 Provisioning API
     * @throws IOException
     * @throws ParseException
     */
    public static Response createSubscriptionAPIM(JSONObject jsonBody) throws Exception {
        return createSubscription(jsonBody, setDefaultHeadersAPIM());
    }

    /**
     * Executes a GET request to Provisioning V3 getJobStatus API and returns response
     * @param correlationId
     * @return
     * @throws Exception
     */
    public static String getJobStatus(String correlationId) throws Exception {
        HashMap<String, String> headers = setDefaultHeadersWorkflow();

        Response response = new Request(ApiEndpoints.PROVISIONING_V3_BASE_URL)
                .setEndPoint(ApiEndpoints.GET_JOB_STATUS + "/" + correlationId)
                .setMethod(Method.GET)
                .setHeader(headers)
                .execute();

        Assert.assertTrue(response.statusCode() == HttpURLConnection.HTTP_OK,
                "GET Job Status API returned " + response.statusCode() + " response");
        return response.body().jsonPath().getString("status");
    }

    /**
     * Executes a GET request to Provisioning V3 getJobStatus DEBUG API and returns response
     * @param correlationId
     * @return
     * @throws Exception
     */
    public static Response getJobStatusDebug(String correlationId) throws Exception {
        HashMap<String, String> headers = setDefaultHeadersWorkflow();

        Response response = new Request(ApiEndpoints.PROVISIONING_V3_BASE_URL)
                .setEndPoint(ApiEndpoints.GET_JOB_STATUS_DEBUG + "/" + correlationId)
                .setMethod(Method.GET)
                .setHeader(headers)
                .execute();

        return response;
    }

    public static void productNotificationAcknowledgement(String eventId) throws Exception {
        HashMap<String, String> headers = setDefaultHeadersWorkflow();
        HashMap<String, String> bodyParams = new HashMap<>();
        bodyParams.put("eventId", eventId);
        bodyParams.put("status", "COMPLETE");

        Response response = new Request(ApiEndpoints.PROVISIONING_V3_BASE_URL)
                .setEndPoint(ApiEndpoints.PRODUCT_NOTIFICATION_ACKNOWLEGDEMENT)
                .setMethod(Method.POST)
                .setHeader(headers)
                .setBodyParams(bodyParams)
                .execute();
    }

    /**
     * Executes get requests to getJobStatus and completes ProductNotification to mark the jobStatus as success
     * @param response
     */
    public static void markJobStatusAsComplete(Response response) throws Exception {
        String correlationId = response.body().jsonPath().getString("correlationId");
        String jobStatus = getJobStatus(correlationId);

        if(jobStatus.equalsIgnoreCase("success")){
            return;
        }else if(jobStatus.equalsIgnoreCase("InProgress")){
            Response debugResponse = getJobStatusDebug(correlationId);
            String eventId = debugResponse.body().jsonPath().getString("eventid");
            productNotificationAcknowledgement(eventId);
        }

        String newJobStatus = getJobStatus(correlationId);

        Assert.assertTrue(newJobStatus.equalsIgnoreCase("success"), "Job status for correlationId " + correlationId + " is " + jobStatus);
    }

}
