package utils;

import constants.AuthAndCredentials;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Base64;
import java.util.HashMap;

public class ApiHelper {

    /**
     * Parses JSON from resources and returns a Hashmap
     * @param fileName
     * @return JsonObject
     * @throws IOException
     * @throws ParseException
     */
    public static JSONObject readJSON(String fileName, String productName) throws Exception {
        File file;
        try{
            file = new File(ClassLoader.getSystemClassLoader().getResource(productName + "/" + fileName).getFile());
        }catch(NullPointerException nullPointerException){
            String nullPointerError = nullPointerException.getMessage();
            throw new Exception("JSON file to be used for request payload could not be located...!!" + "\n" + nullPointerError);
        }
        Reader fileReader = new FileReader(file);
        JSONParser jsonParser = new JSONParser();
        return (JSONObject)jsonParser.parse(fileReader);
    }

    /**
     * Parses JSON from resources and returns a Hashmap
     * @param fileName
     * @return JsonObject
     * @throws IOException
     * @throws ParseException
     */
    public static JSONObject readJSON(String fileName) throws IOException, ParseException {
        File file = new File(ClassLoader.getSystemClassLoader().getResource(fileName).getFile());
        Reader fileReader = new FileReader(file);
        JSONParser jsonParser = new JSONParser();
        return (JSONObject)jsonParser.parse(fileReader);
    }

    /**
     * Reads authAndCredentials.json and returns Basic Auth
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getBasicAuth() throws IOException, ParseException {
        byte[] decodedByteAuth = Base64.getDecoder().decode(AuthAndCredentials.BASIC_AUTH);

        return new String(decodedByteAuth);
    }

    /**
     * Reads authAndCredentials.json and returns Guam username
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getGUAMusername() throws IOException, ParseException {
        byte[] decodedByteUsername = Base64.getDecoder().decode(AuthAndCredentials.GUAM_USERNAME);

        return new String(decodedByteUsername);
    }
    /**
     * Reads authAndCredentials.json and returns Guam password
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getGUAMpassword() throws IOException, ParseException {
        byte[] decodedBytePassword = Base64.getDecoder().decode(AuthAndCredentials.GUAM_PASSWORD);

        return new String(decodedBytePassword);
    }
}
