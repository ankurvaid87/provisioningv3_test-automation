package utils;

import constants.Products;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.UUID;

public class TestData {

    private final String uuid = UUID.randomUUID().toString();
    private final String [] parts = uuid.split("-");
    private final LocalDate date = LocalDate.now();
    private JSONObject jsonBody;
    private String userEmail;
    private static String transactionId;
    private Logger logger = LogManager.getLogger(TestData.class);

    public TestData(String fileName, String productName) throws Exception {
        jsonBody = prepareTestDataFromFile(fileName, productName);
    }

    /**
     * @author : Ankur Vaid
     * Description : Generates a random email address to be used as test data
     * @return
     */
    public String generateRandomEmail(){
        if(userEmail == null || userEmail.isBlank()){
            String randomEmail = "prov"+date+"_"+ parts[0] +"."+ parts[1] +"@mailinator.com";
            return randomEmail;
        }else{
            return userEmail;
        }
    }

    /**
     * @author : Ankur Vaid
     * Description : Generates a random transactionId to be used as test data
     * @return
     */
    public static String generateRandomTransactionId(){
        if(transactionId == null || transactionId.isBlank()){
            LocalDate date = LocalDate.now();
            String uuid = UUID.randomUUID().toString();
            String [] parts = uuid.split("-");

            transactionId = date +"_"+ parts[1] + "_v2.5Prov_xid";
            return transactionId;
        }
        return transactionId;
    }

    /**
     * Parses JSON Body and replaces email in subscriptionAdmin object with a randomly generated email
     * @param jsonObject
     */
    public void replaceRandomEmailInSubscriptionAdmin(JSONObject jsonObject){
        userEmail = generateRandomEmail();

        HashMap<String, String> hm = (HashMap<String, String>)(jsonObject.get("subscriptionAdmin"));
        hm.put("email", userEmail);

        jsonObject.put("subscriptionAdmin", hm);
    }

    public JSONObject prepareTestDataFromFile(String fileName, String productName) throws Exception {
        JSONObject jsonObject = ApiHelper.readJSON(fileName, productName);

        replaceRandomEmailInSubscriptionAdmin(jsonObject);
        assignRandomCorrelationIdToPayload(jsonObject);

        logger.debug("JSON Body for request payload is ->\n" + jsonObject.toJSONString());
        return jsonObject;
    }

    public void assignRandomCorrelationIdToPayload(JSONObject jsonObject){
        String randomCorrelationId = uuid.replaceAll("-", "");

        jsonObject.put("correlationId", randomCorrelationId);
    }

    /**
     * Reads file from test resources and returns JSONObject
     * @param fileName
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public JSONObject getProdNotificationAcknowledgementPayload(String fileName) throws Exception {
        JSONObject jsonObject = ApiHelper.readJSON(fileName, Products.PRODUCT_NOTIFICATION_ACKNOWLEDGEMENT);

        return jsonObject;
    }

    public JSONObject getJsonBody(){
        return jsonBody;
    }

    public String getGeneratedUserEmail(){
        return userEmail;
    }

    public String getGeneratedTransactionId(){
        return transactionId;
    }
}

